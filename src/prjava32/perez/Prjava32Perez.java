package prjava32.perez;
import java.net.InetAddress;
import java.net.UnknownHostException;

//@author perez
public class Prjava32Perez 
{
    public static void main(String[] args) 
    {
        System.out.println("Projecte de Manu Perez");
        System.out.println("Versio 0.1 del projecte prjava32-Perez");
        try
        {
            InetAddress addr = InetAddress.getLocalHost();
            byte[] ipAddr = addr.getAddress();
            String hostname = addr.getHostName();
            System.out.println("Hostname: " + hostname);
            System.out.println("Nom usuario: " + System.getProperty("user.name"));
            System.out.println("Carpeta personal: " + System.getProperty("user.home"));
            System.out.println("Sistema operativo: " + System.getProperty("os.name"));
            System.out.println("Versio OS: " + System.getProperty("os.version"));
        }
        catch(UnknownHostException e)
        {
            e.printStackTrace();
        }
    }
}
